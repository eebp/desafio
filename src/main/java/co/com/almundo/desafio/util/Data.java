package co.com.almundo.desafio.util;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import org.apache.log4j.Logger;
import co.com.almundo.desafio.model.Employee;
import co.com.almundo.desafio.model.Profile;

/**
 * Class to load data init
 * 
 * @author eyder
 *
 */
public class Data {

	final static Logger LOG = Logger.getLogger(Data.class);

	public BlockingQueue<FIFOEntry<Employee>> loadDataInitEmployees() {

		// First, create and load data to initialize system

		// Create profiles
		Profile profile1 = new Profile(1, "Operador", "Perfil para el Empleado básico del Call Center");
		Profile profile2 = new Profile(2, "Supervisor", "Perfil para el Empleado que supervisa el Call Center");
		Profile profile3 = new Profile(3, "Director",
				"Perfil para el Empleado que es responsable de toda la operación del Call Center");

		// Create employees
		Employee employee1 = new Employee(1, "Eyder", "Ascuntar", profile1);
		Employee employee2 = new Employee(2, "Juan", "López", profile1);
		Employee employee3 = new Employee(3, "Pedro", "Paz", profile1);
		Employee employee4 = new Employee(4, "Carlos", "Muñoz", profile2);
		Employee employee5 = new Employee(5, "Guillermo", "Gómez", profile3);

		// define size of queue of employees
		BlockingQueue<FIFOEntry<Employee>> queueEmployees = new PriorityBlockingQueue<>(Constants.NUMBER_OF_EMPLOYEES);

		try {
			// load employees to queue
			queueEmployees.put(new FIFOEntry<>(employee5));
			queueEmployees.put(new FIFOEntry<>(employee4));
			queueEmployees.put(new FIFOEntry<>(employee2));
			queueEmployees.put(new FIFOEntry<>(employee3));
			queueEmployees.put(new FIFOEntry<>(employee1));
		} catch (InterruptedException e) {
			LOG.error("  xxxxxx Error on Data Init  " + e.getMessage());
			e.printStackTrace();
		}
		return queueEmployees;
	}

	public BlockingQueue<FIFOEntry<Employee>> loadOneEmployee() {

		// First, create and load data to initialize system
		// Create profiles
		Profile profile1 = new Profile(1, "Operador", "Perfil para el Empleado básico del Call Center");
		// Create employees
		Employee employee1 = new Employee(1, "Eyder", "Ascuntar", profile1);
		// define size of queue of employees
		BlockingQueue<FIFOEntry<Employee>> queueEmployees = new PriorityBlockingQueue<>(1);
		try {
			// load employees to queue
			queueEmployees.put(new FIFOEntry<>(employee1));
		} catch (InterruptedException e) {
			LOG.error("  xxxxxx Error on Data Init  " + e.getMessage());
			e.printStackTrace();
		}
		return queueEmployees;
	}
}
