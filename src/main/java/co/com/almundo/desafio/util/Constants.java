package co.com.almundo.desafio.util;

public class Constants {

	public static final Integer MIN_TIME_CALL = 5;
	public static final Integer MAX_TIME_CALL = 10;
	public static final Integer TIME_WAIT_TO_AGENT = 30;
	public static final Integer CONCURRENT_CALLS = 10;
	public static final Integer NUMBER_OF_EMPLOYEES = 5;
}
