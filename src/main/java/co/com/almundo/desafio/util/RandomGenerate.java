package co.com.almundo.desafio.util;

import java.util.Random;

/**
 * Util to generate random numbers
 * 
 * @author eyder
 *
 */

public class RandomGenerate {

	public int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
}
