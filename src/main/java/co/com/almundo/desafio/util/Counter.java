package co.com.almundo.desafio.util;

import java.util.concurrent.atomic.*;

/**
 * Class to managed atomic vars
 * 
 * @author eyder
 *
 */

public class Counter {
	private AtomicInteger value = new AtomicInteger();

	public void increment() {
		value.incrementAndGet();
	}

	public void decrement() {
		value.decrementAndGet();
	}

	public int get() {
		return value.get();
	}
}