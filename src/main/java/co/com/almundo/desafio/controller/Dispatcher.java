package co.com.almundo.desafio.controller;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import co.com.almundo.desafio.model.Call;
import co.com.almundo.desafio.model.Employee;
import co.com.almundo.desafio.util.FIFOEntry;

/**
 * 
 * @author eyder
 *
 */
public interface Dispatcher {

	/**
	 * Function to make one call to Callcenter
	 * 
	 * @param call
	 *            object
	 * @param queueEmployees
	 *            list of employees avaiables to atend to clients
	 * @return call object update
	 * @throws InterruptedException
	 */
	Future<Call> dispatchCall(Call call, BlockingQueue<FIFOEntry<Employee>> queueEmployees) throws InterruptedException;

	/**
	 * Function to make several calls redundant to Callcenter
	 * 
	 * @param calls
	 *            list of calls
	 * @param queueEmployees
	 *            list of employees avaiables to atend to clients
	 * @return list of calls update
	 * @throws InterruptedException
	 */
	List<Future<Call>> dispatchCalls(List<Call> calls, BlockingQueue<FIFOEntry<Employee>> queueEmployees)
			throws InterruptedException;

}
