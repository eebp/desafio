package co.com.almundo.desafio.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import co.com.almundo.desafio.model.Call;
import co.com.almundo.desafio.model.Employee;
import co.com.almundo.desafio.util.Constants;
import co.com.almundo.desafio.util.FIFOEntry;

public class DispatcherImpl implements Dispatcher {

	final static Logger LOG = Logger.getLogger(DispatcherImpl.class);
	private ExecutorService pool = Executors.newFixedThreadPool(Constants.CONCURRENT_CALLS);

	@Override
	public Future<Call> dispatchCall(Call call, BlockingQueue<FIFOEntry<Employee>> queueEmployees)
			throws InterruptedException {
		return pool.submit(new CallCenter(queueEmployees, call));
	}

	@Override
	public List<Future<Call>> dispatchCalls(List<Call> calls, BlockingQueue<FIFOEntry<Employee>> queueEmployees)
			throws InterruptedException {
		List<Callable<Call>> taskList = new ArrayList<Callable<Call>>();
		for (Call call : calls) {
			taskList.add((new CallCenter(queueEmployees, call)));
		}
		return pool.invokeAll(taskList);
	}

}
