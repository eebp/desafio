package co.com.almundo.desafio.controller;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import co.com.almundo.desafio.model.Call;
import co.com.almundo.desafio.model.Employee;
import co.com.almundo.desafio.util.Constants;
import co.com.almundo.desafio.util.FIFOEntry;
import co.com.almundo.desafio.util.RandomGenerate;

public class CallCenter implements Callable<Call> {

	final static Logger LOG = Logger.getLogger(CallCenter.class);

	private BlockingQueue<FIFOEntry<Employee>> queueEmployees;
	private Call call;

	public CallCenter(BlockingQueue<FIFOEntry<Employee>> queueEmployees, Call call) {
		this.queueEmployees = queueEmployees;
		this.call = call;
	}

	@Override
	public Call call() throws Exception {

		Employee employeeToAttend = null;
		FIFOEntry<Employee> entry = null;
		try {
			// first get employee to attend this call
			if (queueEmployees != null) {
				if (!queueEmployees.isEmpty()) {
					entry = queueEmployees.poll();
				} else {
					// Expect another employee to be free to answer the call
					entry = queueEmployees.poll(Constants.TIME_WAIT_TO_AGENT, TimeUnit.SECONDS);
					LOG.info(this.call.getClient().getFirstName() + " is waiting for an Agent");
				}
				if (entry != null)
					employeeToAttend = entry.getEntry();
				if (employeeToAttend != null) {
					this.call.setEmployee(employeeToAttend);
					this.call = answerCall(this.call);
				} else {
					this.call.setState("Failed, there are no agents available");
				}
			} else {
				this.call.setState("Failed, there are no agents available");
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (employeeToAttend != null) {
				// Return employee to queue
				queueEmployees.offer(new FIFOEntry<>(call.getEmployee()));
			}
		}

		return this.call;
	}

	public Call answerCall(Call call) {
		RandomGenerate random = new RandomGenerate();
		Integer duration = random.getRandomNumberInRange(Constants.MIN_TIME_CALL, Constants.MAX_TIME_CALL);
		LOG.info(" Start Call from " + call.getClient().getFirstName() + " " + call.getClient().getLastName()
				+ " attended by the " + call.getEmployee().getProfile().getName() + " "
				+ call.getEmployee().getFirstName() + " " + call.getEmployee().getLastName());
		try {
			Thread.sleep((duration * 1000));
			call.setDuration(duration);
			call.setState("Attended");
			LOG.info(" Call from " + call.getClient().getFirstName() + " " + call.getClient().getLastName()
					+ " attended by the " + call.getEmployee().getProfile().getName() + " "
					+ call.getEmployee().getFirstName() + " " + call.getEmployee().getLastName() + " is finalized in "
					+ duration + " seconds");
			queueEmployees.offer(new FIFOEntry<>(call.getEmployee()));
		} catch (InterruptedException e) {
			e.printStackTrace();
			// Return employee to queue
			queueEmployees.offer(new FIFOEntry<>(call.getEmployee()));
		}
		return call;

	}
}
