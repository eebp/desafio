package co.com.almundo.desafio.model;

/**
 * Object that represents one Client of Call Center
 * 
 * @author eyder
 *
 */
public class Client extends Person {

	public Client(Integer id, String firstName, String lastName) {
		super(id, firstName, lastName);
	}

	@Override
	public String toString() {
		return "Client [Id=" + getId() + ", FirstName=" + getFirstName() + ", LastName()=" + getLastName() + "]";
	}

}
