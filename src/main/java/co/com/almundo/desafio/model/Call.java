package co.com.almundo.desafio.model;

/**
 * Object that represents one call into call center
 * 
 * @author eyder
 *
 */
public class Call {

	private Client client;
	private Integer duration;
	private String state;
	private Employee employee;

	public Call() {

	}

	public Call(Client client, String state) {
		super();
		this.client = client;
		this.state = state;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
