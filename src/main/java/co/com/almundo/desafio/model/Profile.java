package co.com.almundo.desafio.model;

/**
 * Object that represents an employee profile in the system
 * @author eyder
 *
 */
public class Profile {

	private Integer id;
	private String name;
	private String description;

	public Profile() {
	}

	public Profile(Integer id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Profile [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

}
