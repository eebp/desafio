package co.com.almundo.desafio.model;

/**
 * Object that represents one Employee of Contact Center
 * 
 * @author eyder
 *
 */

public class Employee extends Person implements Comparable<Employee> {

	private Profile profile;

	public Employee(Integer id, String firstName, String lastName, Profile profile) {
		super(id, firstName, lastName);
		this.profile = profile;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public int compare(Employee employee1, Employee employee2) {
		return employee1.getProfile().getId() - employee2.getProfile().getId();
	}

	@Override
	public String toString() {
		return "Employee [id=" + this.getId() + ", firstName=" + this.getFirstName() + ", lastName="
				+ this.getLastName() + ", profile=" + profile + "]";
	}

	@Override
	public int compareTo(Employee o) {
		return this.getProfile().getId() - o.getProfile().getId();
	}

}
