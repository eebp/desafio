package co.com.almundo.desafio.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import org.apache.log4j.Logger;
import org.junit.Test;
import co.com.almundo.desafio.model.Call;
import co.com.almundo.desafio.model.Client;
import co.com.almundo.desafio.model.Employee;
import co.com.almundo.desafio.util.Data;
import co.com.almundo.desafio.util.FIFOEntry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DispatcherTest {

	final static Logger LOG = Logger.getLogger(DispatcherTest.class);

	@Test
	public void testOneCallMultipleAgents() throws InterruptedException, ExecutionException, TimeoutException {
		LOG.info("  >>>> testOneCallMultipleAgents");
		Data data = new Data();
		BlockingQueue<FIFOEntry<Employee>> queueEmployees = data.loadDataInitEmployees();
		Dispatcher dispatcher = new DispatcherImpl();
		Call call = new Call(new Client(1, "José", "Rosales"), "New");
		Future<Call> callFuture = dispatcher.dispatchCall(call, queueEmployees);
		assertEquals(callFuture.get().getState(), "Attended");
	}

	@Test
	public void testOneCallNotAgents() throws InterruptedException, ExecutionException {
		LOG.info("  >>>> testOneCallNotAgents");
		BlockingQueue<FIFOEntry<Employee>> queueEmployees = null;
		Dispatcher dispatcher = new DispatcherImpl();
		Call call = new Call(new Client(1, "Fredy", "Miranda"), "New");
		Future<Call> callFuture = dispatcher.dispatchCall(call, queueEmployees);
		assertEquals(callFuture.get().getState(), "Failed, there are no agents available");
	}

	@Test
	public void testOneCallOneAgent() throws InterruptedException, ExecutionException {
		LOG.info("  >>>> testOneCallOneAgent");
		Data data = new Data();
		BlockingQueue<FIFOEntry<Employee>> queueEmployees = data.loadOneEmployee();
		Dispatcher dispatcher = new DispatcherImpl();
		Call call = new Call(new Client(1, "Luis", "Eraso"), "New");
		Future<Call> callFuture = dispatcher.dispatchCall(call, queueEmployees);
		assertEquals(callFuture.get().getState(), "Attended");
	}

	@Test
	public void testOneCallOneAgentCheckTime() throws InterruptedException, ExecutionException {
		LOG.info("  >>>> testOneCallOneAgentCheckTime");
		Data data = new Data();
		BlockingQueue<FIFOEntry<Employee>> queueEmployees = data.loadOneEmployee();
		Dispatcher dispatcher = new DispatcherImpl();
		Call call = new Call(new Client(1, "Pepe", "Perez"), "New");
		Future<Call> callFuture = dispatcher.dispatchCall(call, queueEmployees);
		assertTrue(callFuture.get().getDuration() >= 5 || callFuture.get().getDuration() <= 10);
	}

	@Test
	public void testTwelveCallsFiveAgents() throws InterruptedException, ExecutionException {
		LOG.info(" >>>> testTwelveCallsFiveAgents");
		Integer countCallsSuccessful = 0;

		Data data = new Data();
		BlockingQueue<FIFOEntry<Employee>> queueEmployees = data.loadDataInitEmployees();
		Dispatcher dispatcher = new DispatcherImpl();
		List<Call> listCalls = new ArrayList<>();

		Call call1 = new Call(new Client(1, "Aura", "Ligia"), "New");
		listCalls.add(call1);
		Call call2 = new Call(new Client(2, "Maria", "Margarita"), "New");
		listCalls.add(call2);
		Call call3 = new Call(new Client(3, "Aracely", "Rosales"), "New");
		listCalls.add(call3);
		Call call4 = new Call(new Client(4, "Yolanda", "Arciniegas"), "New");
		listCalls.add(call4);
		Call call5 = new Call(new Client(5, "Melva", "Liliana"), "New");
		listCalls.add(call5);
		Call call6 = new Call(new Client(6, "Sandra", "Elizabeth"), "New");
		listCalls.add(call6);
		Call call7 = new Call(new Client(7, "Rocio", "Deyanira"), "New");
		listCalls.add(call7);
		Call call8 = new Call(new Client(8, "Veronica", "Lara"), "New");
		listCalls.add(call8);
		Call call9 = new Call(new Client(9, "Katherine", "Gonzales"), "New");
		listCalls.add(call9);
		Call call10 = new Call(new Client(10, "Alexandra", "Armero"), "New");
		listCalls.add(call10);
		Call call11 = new Call(new Client(11, "Marisol", "Checa"), "New");
		listCalls.add(call11);
		Call call12 = new Call(new Client(12, "Yesenia", "Cardona"), "New");
		listCalls.add(call12);

		List<Future<Call>> listCallFuture = dispatcher.dispatchCalls(listCalls, queueEmployees);
		for (Future<Call> callFuture : listCallFuture) {
			if (callFuture.get().getState().equals("Attended")) {
				countCallsSuccessful++;
			}
		}
		assertEquals(countCallsSuccessful.intValue(), listCalls.size());
	}
}
