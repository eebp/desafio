# Ejercicio Call Center Java #


### Consigna ###
--------
Existe	un	Call	Center	donde	hay	3	tipos	de	empleados:	`Operador`,`Supervisor`	y	`Director`.	El	proceso	de	la	atención	de	una	llamada telefónica	en	primera	instancia	debe	ser	atendida	por	un	operador,	si no	hay	ninguno	libre	debe	ser	atendida	por	un	supervisor,	y	de	no haber	tampoco 	supervisores	libres	debe	ser	atendida	por	un	director.

### Implementación ###
--------
Se optó por utilizar Hilos en Java, esto debido a uno de los requerimientos especificos donde se solicita que se deben poder procesar llamadas de manera concurrente. Toda la logica del negocio se desarrolló entorno a esta consigna.

+ Organización de la aplicación en Paquetes:
    + model: Se encuentra el módelo de datos, en este paquete están los objetos creados  			especicamente para 	  dar solución a los requerimientos de la aplicación.
    + controller: Se encuentran los controladores del módelo de negocio, en este paquete se definió la interface `Dispatcher`, `DispatcherImpl` y la clase `CallCenter` la cual implementa la interfaz Callable con el fin de obtener el resultado de la ejecución de los hilos en una variable de tipo Future.  
    + util: Se encuentran utilidades para la aplicación como la generación de números aleatorios, valores constantes, cargas iniciales de datos.
    
    
+ Orden de respuesta según tipo de Empleado:
	Se creó un objeto Perfil para ser asociado a un empleado, el perfil va a dar la prioridad para atención de llamadas por empleado haciendo uso de la interfaz `Compare`. Los empleados son almacenados en una Cola de tipo `PriorityBlockingQueue` dando prioridad primero a aquellos empleados con un perfil menor, para nuestro caso los Operadores.
	Adicional a esto se utilizó la clase `FIFOEntry` esta funciona como un complemento perfecto ya que respeta el orden de prioridad definido por los roles y posibilita el orden `First Input - First Output` para aquellos empleados que tengan el mismo perfil.
	
+ Llamadas entre 5 y 10 segundos:
	Se utilizó una clase para la generación de numeros aleatorios entre 5 y 10 segundos, tiempo que se utiliza para `Dormir` el hilo de cada una de las llamadas, simulando el tiempo que está el cliente al teléfono.
	
+ Clase Dispatcher:
	Se creó la interface `Dispatcher` con 2 metodos `dispatchCall`, el cual recibe un objeto de tipo `Call` y la cola de empleados disponibles para responder las llamadas en ese momento, el segundo metodo es `dispatchCalls`este a diferencia de su predecesor puede reciber una lista de llamadas con el fin de ejecutarlas de manera concurrente, de esta manera se da solución a las 10 o n llamadas concurrentes, la implementación de esta interface está hecha en la clase `DispatcherImpl`, aquí se definió un objeto de tipo `ExecutorService` donde definimos mediante una propiedad de la clase `Constants` cuantos hilos de llamadas pueden ser concurrentes en la aplicación.
	
+ Clase CallCenter:
	Esta clase implementa la interface `Callable` lo que nos permite realizar la ejecución de los hilos que contiene el pool y esperar los datos actualizados en el Objeto Call, esto mediante un objeto de tipo Future.
	+ Procceso de Llamada: 
		+ Recibe el objeto llamada y la cola de empleados. 
		+ Verifica si existe algun empleado disponible, como ya hemos ordenado la cola por prioridad, el primer empleado disponible que encuentre será aquel con el menor nivel de importancia según su perfil.
		+ Si encuentra un empleado, se ejecuta la función answerCall, obteniendo un valor aleatorio para la duración de la llamada (entre el valor minimo y maximo definido en las Constantes).
		+ Se agrega el empleado asignado a esta llamada
		+ Se simula una llamada con este tiempo colocando a dormir el hilo
		+ Se actualiza el estado de la llamada `Attended` y su duración 
		+ Retorna el empleado a la cola de Disponible
		+ Se retorna la llamada
		+ En caso de no encontrar un agente disponible, espera un momento(valor definido en las Constantes) por un agente, con esto solucionamos uno de los requerimientos de la consigna
		+ Si pasado este tiempo un agente vuelve a estar disponible, volvemos a los pasos anteriores de un flujo normal
		+ Si aun después de esperar no encuentra un agente, retorna la llamada con estado `Failed, there are no agents available`
		+ Para el caso en que son llamadas conccurrentes se retorna una lista de Objetos de tipo Future con las llamadas actualizadas según lo procesado.

### Pruebas ###
--------
Se realizarón las pruebas unitarias con JUnit versión 4.12, la clase se llama `DispatcherTest`